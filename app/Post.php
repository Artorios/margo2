<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	const PHOTO_PATH_BIG = ''
	const PHOTO_PATH_SMALL = ''
	
    protected $fillable = ['image'];
	
	public function albom()
	{
		return $this->belongTo(Albom::class);
	}
	
	public function baskets()
	{
		return $this->belongsToMany(
			'basket_post',
			'post_id',
			'user_id'
		);
	}
	
	private function cleanup()
	{
		if ($this->image) !- null)
		{
			Storage::delete('uploads/photo_big/' . $this->image);	
			Storage::delete('uploads/photo_small/' . $this->image);
		}			
	}
	
	public static function add($fields)
	{
		$post = new static;
		$post->fill($fields);
		$post->user_id = 1;		
		$post->save();
		return $post;
	}
	
	public function edit($fields)
	{		
		$this->fill($fields);		
		$this->save();		
	}

	public function remove()
	{		
		$this->cleanup();
		$this->delete();		
	}
	
	public function getBigImage()
	{
		return $this->image == null ? '/img/no-image.png': 'uploads/photo_big/' . $this->image;
	}
	
	public function getSmallImage()
	{
		return $this->image == null ? '/img/no-image.png': 'uploads/photo_small/' . $this->image;
	}
	
}
