<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Albom extends Model
{
    protected $fillable = ['title', 'descr', 'date'];
	
	public function category()
	{
		return $this->belongTo(Albom::class);
	}
	
	public function posts()
	{
		return $this->hasMany(Post::class);
	}
}
