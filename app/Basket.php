<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    public function posts()
	{
		return $this->belongsToMany(
			'basket_post',
			'user_id',
			'post_id'
		);
	}
}
