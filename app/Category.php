<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title', 'descr', 'price'];
	
	public function alboms()
	{
		return $this->hasMany(Albom::class);
	}
	
	public static function add($fields)
	{
		$category = new static;
		$category->fill($fields);
		$category->save();
		return $category;
	}
	
	public function edit($fields)
	{		
		$this->fill($fields);
		$this->save();		
	}

	public function remove()
	{		
		//очистить категория->альбомы->посты
		$this->delete();
	}
	
	public function getTitle()
	{
		return $this->title;
	}

	public function getPrice()
	{
		return $this->price;
	}
	
}
