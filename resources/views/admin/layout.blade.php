<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Blank Page</title>
	@include('admin.head')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

		@include('admin.main-header')

        <!-- Left side column. contains the sidebar -->
		@include('admin.sidebar-left')
        
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
				@yield('page-header')
				
				@include('admin.errors')
            </section>

            <!-- Main content -->
            <section class="content">
					@yield('content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
		
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.3.7
            </div>
            <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com/">Almsaeed Studio</a>.</strong> All rights reserved.
        </footer>

    </div><!-- ./wrapper -->
	@include('admin.scripts')
</body>

</html>