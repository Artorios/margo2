@extends('admin.layout')

@section('page-header')
	<h1>Привет! Это админка<small>Категории</small></h1>
@endsection

@section('content')
	<!-- Default box -->
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Листинг сущности</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="form-group">
				<a href="{{route('cat.create')}}" class="btn btn-success">Добавить</a>
			</div>
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Название</th>
						<th>Описание</th>
						<th>Цена</th>
						<th>Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{$category->id}}</td>
						<td>{{$category->title}}</td>
						<td>
							<a href="{{--route('categories.edit', $category->id)--}}" class="fa fa-pencil"></a>

							{{Form::open(['route'=>['categories.destroy', $category->id], 'method'=>'delete'])}}
							<button onclick="return confirm('are you sure?')" type="submit" class="delete">
								<i class="fa fa-remove"></i>
							</button>

							{{Form::close()}}

						</td>
					</tr>
					@endforeach
				</tbody>
					<tr>
						<th>ID</th>
						<th>Название</th>
						<th>Описание</th>
						<th>Цена</th>
						<th>Действия</th>
					</tr>				
				<tfoot>
				</tfoot>
			</table>
		</div>
		<!-- /.box-body -->
	</div>
@endsection